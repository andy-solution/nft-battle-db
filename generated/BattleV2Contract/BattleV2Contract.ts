// THIS IS AN AUTOGENERATED FILE. DO NOT EDIT THIS FILE DIRECTLY.

import {
  ethereum,
  JSONValue,
  TypedMap,
  Entity,
  Bytes,
  Address,
  BigInt
} from "@graphprotocol/graph-ts";

export class ClaimedNFT extends ethereum.Event {
  get params(): ClaimedNFT__Params {
    return new ClaimedNFT__Params(this);
  }
}

export class ClaimedNFT__Params {
  _event: ClaimedNFT;

  constructor(event: ClaimedNFT) {
    this._event = event;
  }

  get nftId(): BigInt {
    return this._event.parameters[0].value.toBigInt();
  }

  get nftOwner(): Address {
    return this._event.parameters[1].value.toAddress();
  }
}

export class ClaimedPresent extends ethereum.Event {
  get params(): ClaimedPresent__Params {
    return new ClaimedPresent__Params(this);
  }
}

export class ClaimedPresent__Params {
  _event: ClaimedPresent;

  constructor(event: ClaimedPresent) {
    this._event = event;
  }

  get nftId(): BigInt {
    return this._event.parameters[0].value.toBigInt();
  }

  get nftOwner(): Address {
    return this._event.parameters[1].value.toAddress();
  }

  get amount(): BigInt {
    return this._event.parameters[2].value.toBigInt();
  }
}

export class CompleteBattle extends ethereum.Event {
  get params(): CompleteBattle__Params {
    return new CompleteBattle__Params(this);
  }
}

export class CompleteBattle__Params {
  _event: CompleteBattle;

  constructor(event: CompleteBattle) {
    this._event = event;
  }

  get battleId(): BigInt {
    return this._event.parameters[0].value.toBigInt();
  }
}

export class NewBattleCreated extends ethereum.Event {
  get params(): NewBattleCreated__Params {
    return new NewBattleCreated__Params(this);
  }
}

export class NewBattleCreated__Params {
  _event: NewBattleCreated;

  constructor(event: NewBattleCreated) {
    this._event = event;
  }

  get battleId(): BigInt {
    return this._event.parameters[0].value.toBigInt();
  }

  get uri(): string {
    return this._event.parameters[1].value.toString();
  }

  get level(): BigInt {
    return this._event.parameters[2].value.toBigInt();
  }

  get award(): BigInt {
    return this._event.parameters[3].value.toBigInt();
  }

  get startTime(): BigInt {
    return this._event.parameters[4].value.toBigInt();
  }

  get endTime(): BigInt {
    return this._event.parameters[5].value.toBigInt();
  }
}

export class NewParticipantJoined extends ethereum.Event {
  get params(): NewParticipantJoined__Params {
    return new NewParticipantJoined__Params(this);
  }
}

export class NewParticipantJoined__Params {
  _event: NewParticipantJoined;

  constructor(event: NewParticipantJoined) {
    this._event = event;
  }

  get battleId(): BigInt {
    return this._event.parameters[0].value.toBigInt();
  }

  get nftId(): BigInt {
    return this._event.parameters[1].value.toBigInt();
  }

  get total(): BigInt {
    return this._event.parameters[2].value.toBigInt();
  }
}

export class NewWinner extends ethereum.Event {
  get params(): NewWinner__Params {
    return new NewWinner__Params(this);
  }
}

export class NewWinner__Params {
  _event: NewWinner;

  constructor(event: NewWinner) {
    this._event = event;
  }

  get battleId(): BigInt {
    return this._event.parameters[0].value.toBigInt();
  }

  get nftId(): BigInt {
    return this._event.parameters[1].value.toBigInt();
  }

  get nftOwner(): Address {
    return this._event.parameters[2].value.toAddress();
  }
}

export class OwnershipTransferred extends ethereum.Event {
  get params(): OwnershipTransferred__Params {
    return new OwnershipTransferred__Params(this);
  }
}

export class OwnershipTransferred__Params {
  _event: OwnershipTransferred;

  constructor(event: OwnershipTransferred) {
    this._event = event;
  }

  get previousOwner(): Address {
    return this._event.parameters[0].value.toAddress();
  }

  get newOwner(): Address {
    return this._event.parameters[1].value.toAddress();
  }
}

export class RequestRNG extends ethereum.Event {
  get params(): RequestRNG__Params {
    return new RequestRNG__Params(this);
  }
}

export class RequestRNG__Params {
  _event: RequestRNG;

  constructor(event: RequestRNG) {
    this._event = event;
  }

  get battleId(): BigInt {
    return this._event.parameters[0].value.toBigInt();
  }

  get requestId(): Bytes {
    return this._event.parameters[1].value.toBytes();
  }
}

export class ResponseRNG extends ethereum.Event {
  get params(): ResponseRNG__Params {
    return new ResponseRNG__Params(this);
  }
}

export class ResponseRNG__Params {
  _event: ResponseRNG;

  constructor(event: ResponseRNG) {
    this._event = event;
  }

  get battleId(): BigInt {
    return this._event.parameters[0].value.toBigInt();
  }

  get randomness(): BigInt {
    return this._event.parameters[1].value.toBigInt();
  }

  get requestId(): Bytes {
    return this._event.parameters[2].value.toBytes();
  }
}

export class WithdrawBalance extends ethereum.Event {
  get params(): WithdrawBalance__Params {
    return new WithdrawBalance__Params(this);
  }
}

export class WithdrawBalance__Params {
  _event: WithdrawBalance;

  constructor(event: WithdrawBalance) {
    this._event = event;
  }

  get target(): Address {
    return this._event.parameters[0].value.toAddress();
  }

  get token(): Address {
    return this._event.parameters[1].value.toAddress();
  }

  get amount(): BigInt {
    return this._event.parameters[2].value.toBigInt();
  }
}

export class BattleV2Contract__battleHistoriesResult {
  value0: BigInt;
  value1: Address;
  value2: boolean;
  value3: boolean;

  constructor(
    value0: BigInt,
    value1: Address,
    value2: boolean,
    value3: boolean
  ) {
    this.value0 = value0;
    this.value1 = value1;
    this.value2 = value2;
    this.value3 = value3;
  }

  toMap(): TypedMap<string, ethereum.Value> {
    let map = new TypedMap<string, ethereum.Value>();
    map.set("value0", ethereum.Value.fromUnsignedBigInt(this.value0));
    map.set("value1", ethereum.Value.fromAddress(this.value1));
    map.set("value2", ethereum.Value.fromBoolean(this.value2));
    map.set("value3", ethereum.Value.fromBoolean(this.value3));
    return map;
  }
}

export class BattleV2Contract__battlesResult {
  value0: BigInt;
  value1: string;
  value2: BigInt;
  value3: BigInt;
  value4: BigInt;
  value5: BigInt;
  value6: i32;

  constructor(
    value0: BigInt,
    value1: string,
    value2: BigInt,
    value3: BigInt,
    value4: BigInt,
    value5: BigInt,
    value6: i32
  ) {
    this.value0 = value0;
    this.value1 = value1;
    this.value2 = value2;
    this.value3 = value3;
    this.value4 = value4;
    this.value5 = value5;
    this.value6 = value6;
  }

  toMap(): TypedMap<string, ethereum.Value> {
    let map = new TypedMap<string, ethereum.Value>();
    map.set("value0", ethereum.Value.fromUnsignedBigInt(this.value0));
    map.set("value1", ethereum.Value.fromString(this.value1));
    map.set("value2", ethereum.Value.fromUnsignedBigInt(this.value2));
    map.set("value3", ethereum.Value.fromUnsignedBigInt(this.value3));
    map.set("value4", ethereum.Value.fromUnsignedBigInt(this.value4));
    map.set("value5", ethereum.Value.fromUnsignedBigInt(this.value5));
    map.set(
      "value6",
      ethereum.Value.fromUnsignedBigInt(BigInt.fromI32(this.value6))
    );
    return map;
  }
}

export class BattleV2Contract__participantsResult {
  value0: BigInt;
  value1: Address;
  value2: boolean;
  value3: boolean;

  constructor(
    value0: BigInt,
    value1: Address,
    value2: boolean,
    value3: boolean
  ) {
    this.value0 = value0;
    this.value1 = value1;
    this.value2 = value2;
    this.value3 = value3;
  }

  toMap(): TypedMap<string, ethereum.Value> {
    let map = new TypedMap<string, ethereum.Value>();
    map.set("value0", ethereum.Value.fromUnsignedBigInt(this.value0));
    map.set("value1", ethereum.Value.fromAddress(this.value1));
    map.set("value2", ethereum.Value.fromBoolean(this.value2));
    map.set("value3", ethereum.Value.fromBoolean(this.value3));
    return map;
  }
}

export class BattleV2Contract extends ethereum.SmartContract {
  static bind(address: Address): BattleV2Contract {
    return new BattleV2Contract("BattleV2Contract", address);
  }

  BATTLE_TIME(): BigInt {
    let result = super.call("BATTLE_TIME", "BATTLE_TIME():(uint256)", []);

    return result[0].toBigInt();
  }

  try_BATTLE_TIME(): ethereum.CallResult<BigInt> {
    let result = super.tryCall("BATTLE_TIME", "BATTLE_TIME():(uint256)", []);
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  battleHistories(param0: BigInt): BattleV2Contract__battleHistoriesResult {
    let result = super.call(
      "battleHistories",
      "battleHistories(uint256):(uint256,address,bool,bool)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );

    return new BattleV2Contract__battleHistoriesResult(
      result[0].toBigInt(),
      result[1].toAddress(),
      result[2].toBoolean(),
      result[3].toBoolean()
    );
  }

  try_battleHistories(
    param0: BigInt
  ): ethereum.CallResult<BattleV2Contract__battleHistoriesResult> {
    let result = super.tryCall(
      "battleHistories",
      "battleHistories(uint256):(uint256,address,bool,bool)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(
      new BattleV2Contract__battleHistoriesResult(
        value[0].toBigInt(),
        value[1].toAddress(),
        value[2].toBoolean(),
        value[3].toBoolean()
      )
    );
  }

  battleRandomness(param0: BigInt): BigInt {
    let result = super.call(
      "battleRandomness",
      "battleRandomness(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );

    return result[0].toBigInt();
  }

  try_battleRandomness(param0: BigInt): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "battleRandomness",
      "battleRandomness(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  battleWinnerIndexes(param0: BigInt): BigInt {
    let result = super.call(
      "battleWinnerIndexes",
      "battleWinnerIndexes(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );

    return result[0].toBigInt();
  }

  try_battleWinnerIndexes(param0: BigInt): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "battleWinnerIndexes",
      "battleWinnerIndexes(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  battles(param0: BigInt): BattleV2Contract__battlesResult {
    let result = super.call(
      "battles",
      "battles(uint256):(uint256,string,uint256,uint256,uint256,uint256,uint8)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );

    return new BattleV2Contract__battlesResult(
      result[0].toBigInt(),
      result[1].toString(),
      result[2].toBigInt(),
      result[3].toBigInt(),
      result[4].toBigInt(),
      result[5].toBigInt(),
      result[6].toI32()
    );
  }

  try_battles(
    param0: BigInt
  ): ethereum.CallResult<BattleV2Contract__battlesResult> {
    let result = super.tryCall(
      "battles",
      "battles(uint256):(uint256,string,uint256,uint256,uint256,uint256,uint8)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(
      new BattleV2Contract__battlesResult(
        value[0].toBigInt(),
        value[1].toString(),
        value[2].toBigInt(),
        value[3].toBigInt(),
        value[4].toBigInt(),
        value[5].toBigInt(),
        value[6].toI32()
      )
    );
  }

  battlesByLevel(param0: BigInt): BigInt {
    let result = super.call(
      "battlesByLevel",
      "battlesByLevel(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );

    return result[0].toBigInt();
  }

  try_battlesByLevel(param0: BigInt): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "battlesByLevel",
      "battlesByLevel(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  battlesCount(): BigInt {
    let result = super.call("battlesCount", "battlesCount():(uint256)", []);

    return result[0].toBigInt();
  }

  try_battlesCount(): ethereum.CallResult<BigInt> {
    let result = super.tryCall("battlesCount", "battlesCount():(uint256)", []);
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  battlesCountByLevel(param0: BigInt): BigInt {
    let result = super.call(
      "battlesCountByLevel",
      "battlesCountByLevel(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );

    return result[0].toBigInt();
  }

  try_battlesCountByLevel(param0: BigInt): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "battlesCountByLevel",
      "battlesCountByLevel(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  canRetrieveNFT(_battleId: BigInt, _owner: Address): boolean {
    let result = super.call(
      "canRetrieveNFT",
      "canRetrieveNFT(uint256,address):(bool)",
      [
        ethereum.Value.fromUnsignedBigInt(_battleId),
        ethereum.Value.fromAddress(_owner)
      ]
    );

    return result[0].toBoolean();
  }

  try_canRetrieveNFT(
    _battleId: BigInt,
    _owner: Address
  ): ethereum.CallResult<boolean> {
    let result = super.tryCall(
      "canRetrieveNFT",
      "canRetrieveNFT(uint256,address):(bool)",
      [
        ethereum.Value.fromUnsignedBigInt(_battleId),
        ethereum.Value.fromAddress(_owner)
      ]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBoolean());
  }

  createNewBattle(
    _uri: string,
    _level: BigInt,
    _award: BigInt,
    _startTime: BigInt
  ): BigInt {
    let result = super.call(
      "createNewBattle",
      "createNewBattle(string,uint256,uint256,uint256):(uint256)",
      [
        ethereum.Value.fromString(_uri),
        ethereum.Value.fromUnsignedBigInt(_level),
        ethereum.Value.fromUnsignedBigInt(_award),
        ethereum.Value.fromUnsignedBigInt(_startTime)
      ]
    );

    return result[0].toBigInt();
  }

  try_createNewBattle(
    _uri: string,
    _level: BigInt,
    _award: BigInt,
    _startTime: BigInt
  ): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "createNewBattle",
      "createNewBattle(string,uint256,uint256,uint256):(uint256)",
      [
        ethereum.Value.fromString(_uri),
        ethereum.Value.fromUnsignedBigInt(_level),
        ethereum.Value.fromUnsignedBigInt(_award),
        ethereum.Value.fromUnsignedBigInt(_startTime)
      ]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  currentBattleId(): BigInt {
    let result = super.call(
      "currentBattleId",
      "currentBattleId():(uint256)",
      []
    );

    return result[0].toBigInt();
  }

  try_currentBattleId(): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "currentBattleId",
      "currentBattleId():(uint256)",
      []
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  insuranceFee(): BigInt {
    let result = super.call("insuranceFee", "insuranceFee():(uint256)", []);

    return result[0].toBigInt();
  }

  try_insuranceFee(): ethereum.CallResult<BigInt> {
    let result = super.tryCall("insuranceFee", "insuranceFee():(uint256)", []);
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  insurancePeriod(): BigInt {
    let result = super.call(
      "insurancePeriod",
      "insurancePeriod():(uint256)",
      []
    );

    return result[0].toBigInt();
  }

  try_insurancePeriod(): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "insurancePeriod",
      "insurancePeriod():(uint256)",
      []
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  insurances(param0: Address): BigInt {
    let result = super.call("insurances", "insurances(address):(uint256)", [
      ethereum.Value.fromAddress(param0)
    ]);

    return result[0].toBigInt();
  }

  try_insurances(param0: Address): ethereum.CallResult<BigInt> {
    let result = super.tryCall("insurances", "insurances(address):(uint256)", [
      ethereum.Value.fromAddress(param0)
    ]);
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  owner(): Address {
    let result = super.call("owner", "owner():(address)", []);

    return result[0].toAddress();
  }

  try_owner(): ethereum.CallResult<Address> {
    let result = super.tryCall("owner", "owner():(address)", []);
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toAddress());
  }

  participants(
    param0: BigInt,
    param1: BigInt
  ): BattleV2Contract__participantsResult {
    let result = super.call(
      "participants",
      "participants(uint256,uint256):(uint256,address,bool,bool)",
      [
        ethereum.Value.fromUnsignedBigInt(param0),
        ethereum.Value.fromUnsignedBigInt(param1)
      ]
    );

    return new BattleV2Contract__participantsResult(
      result[0].toBigInt(),
      result[1].toAddress(),
      result[2].toBoolean(),
      result[3].toBoolean()
    );
  }

  try_participants(
    param0: BigInt,
    param1: BigInt
  ): ethereum.CallResult<BattleV2Contract__participantsResult> {
    let result = super.tryCall(
      "participants",
      "participants(uint256,uint256):(uint256,address,bool,bool)",
      [
        ethereum.Value.fromUnsignedBigInt(param0),
        ethereum.Value.fromUnsignedBigInt(param1)
      ]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(
      new BattleV2Contract__participantsResult(
        value[0].toBigInt(),
        value[1].toAddress(),
        value[2].toBoolean(),
        value[3].toBoolean()
      )
    );
  }

  participantsCount(param0: BigInt): BigInt {
    let result = super.call(
      "participantsCount",
      "participantsCount(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );

    return result[0].toBigInt();
  }

  try_participantsCount(param0: BigInt): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "participantsCount",
      "participantsCount(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(param0)]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  requestBattleIds(param0: Bytes): BigInt {
    let result = super.call(
      "requestBattleIds",
      "requestBattleIds(bytes32):(uint256)",
      [ethereum.Value.fromFixedBytes(param0)]
    );

    return result[0].toBigInt();
  }

  try_requestBattleIds(param0: Bytes): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "requestBattleIds",
      "requestBattleIds(bytes32):(uint256)",
      [ethereum.Value.fromFixedBytes(param0)]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }
}

export class ConstructorCall extends ethereum.Call {
  get inputs(): ConstructorCall__Inputs {
    return new ConstructorCall__Inputs(this);
  }

  get outputs(): ConstructorCall__Outputs {
    return new ConstructorCall__Outputs(this);
  }
}

export class ConstructorCall__Inputs {
  _call: ConstructorCall;

  constructor(call: ConstructorCall) {
    this._call = call;
  }

  get _vrfCoordinator(): Address {
    return this._call.inputValues[0].value.toAddress();
  }

  get _linkToken(): Address {
    return this._call.inputValues[1].value.toAddress();
  }

  get _keyHash(): Bytes {
    return this._call.inputValues[2].value.toBytes();
  }

  get _fee(): BigInt {
    return this._call.inputValues[3].value.toBigInt();
  }

  get _digits(): BigInt {
    return this._call.inputValues[4].value.toBigInt();
  }
}

export class ConstructorCall__Outputs {
  _call: ConstructorCall;

  constructor(call: ConstructorCall) {
    this._call = call;
  }
}

export class BuyInsuranceCall extends ethereum.Call {
  get inputs(): BuyInsuranceCall__Inputs {
    return new BuyInsuranceCall__Inputs(this);
  }

  get outputs(): BuyInsuranceCall__Outputs {
    return new BuyInsuranceCall__Outputs(this);
  }
}

export class BuyInsuranceCall__Inputs {
  _call: BuyInsuranceCall;

  constructor(call: BuyInsuranceCall) {
    this._call = call;
  }
}

export class BuyInsuranceCall__Outputs {
  _call: BuyInsuranceCall;

  constructor(call: BuyInsuranceCall) {
    this._call = call;
  }
}

export class ClaimPresentCall extends ethereum.Call {
  get inputs(): ClaimPresentCall__Inputs {
    return new ClaimPresentCall__Inputs(this);
  }

  get outputs(): ClaimPresentCall__Outputs {
    return new ClaimPresentCall__Outputs(this);
  }
}

export class ClaimPresentCall__Inputs {
  _call: ClaimPresentCall;

  constructor(call: ClaimPresentCall) {
    this._call = call;
  }

  get _battleId(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }
}

export class ClaimPresentCall__Outputs {
  _call: ClaimPresentCall;

  constructor(call: ClaimPresentCall) {
    this._call = call;
  }
}

export class CompleteBattleCall extends ethereum.Call {
  get inputs(): CompleteBattleCall__Inputs {
    return new CompleteBattleCall__Inputs(this);
  }

  get outputs(): CompleteBattleCall__Outputs {
    return new CompleteBattleCall__Outputs(this);
  }
}

export class CompleteBattleCall__Inputs {
  _call: CompleteBattleCall;

  constructor(call: CompleteBattleCall) {
    this._call = call;
  }

  get _battleId(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }

  get _seed(): BigInt {
    return this._call.inputValues[1].value.toBigInt();
  }
}

export class CompleteBattleCall__Outputs {
  _call: CompleteBattleCall;

  constructor(call: CompleteBattleCall) {
    this._call = call;
  }
}

export class CreateNewBattleCall extends ethereum.Call {
  get inputs(): CreateNewBattleCall__Inputs {
    return new CreateNewBattleCall__Inputs(this);
  }

  get outputs(): CreateNewBattleCall__Outputs {
    return new CreateNewBattleCall__Outputs(this);
  }
}

export class CreateNewBattleCall__Inputs {
  _call: CreateNewBattleCall;

  constructor(call: CreateNewBattleCall) {
    this._call = call;
  }

  get _uri(): string {
    return this._call.inputValues[0].value.toString();
  }

  get _level(): BigInt {
    return this._call.inputValues[1].value.toBigInt();
  }

  get _award(): BigInt {
    return this._call.inputValues[2].value.toBigInt();
  }

  get _startTime(): BigInt {
    return this._call.inputValues[3].value.toBigInt();
  }
}

export class CreateNewBattleCall__Outputs {
  _call: CreateNewBattleCall;

  constructor(call: CreateNewBattleCall) {
    this._call = call;
  }

  get value0(): BigInt {
    return this._call.outputValues[0].value.toBigInt();
  }
}

export class InitializeCall extends ethereum.Call {
  get inputs(): InitializeCall__Inputs {
    return new InitializeCall__Inputs(this);
  }

  get outputs(): InitializeCall__Outputs {
    return new InitializeCall__Outputs(this);
  }
}

export class InitializeCall__Inputs {
  _call: InitializeCall;

  constructor(call: InitializeCall) {
    this._call = call;
  }

  get _nft(): Address {
    return this._call.inputValues[0].value.toAddress();
  }

  get _gfx(): Address {
    return this._call.inputValues[1].value.toAddress();
  }
}

export class InitializeCall__Outputs {
  _call: InitializeCall;

  constructor(call: InitializeCall) {
    this._call = call;
  }
}

export class ParticipateBattleCall extends ethereum.Call {
  get inputs(): ParticipateBattleCall__Inputs {
    return new ParticipateBattleCall__Inputs(this);
  }

  get outputs(): ParticipateBattleCall__Outputs {
    return new ParticipateBattleCall__Outputs(this);
  }
}

export class ParticipateBattleCall__Inputs {
  _call: ParticipateBattleCall;

  constructor(call: ParticipateBattleCall) {
    this._call = call;
  }

  get _battleId(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }

  get _nftId(): BigInt {
    return this._call.inputValues[1].value.toBigInt();
  }
}

export class ParticipateBattleCall__Outputs {
  _call: ParticipateBattleCall;

  constructor(call: ParticipateBattleCall) {
    this._call = call;
  }
}

export class RawFulfillRandomnessCall extends ethereum.Call {
  get inputs(): RawFulfillRandomnessCall__Inputs {
    return new RawFulfillRandomnessCall__Inputs(this);
  }

  get outputs(): RawFulfillRandomnessCall__Outputs {
    return new RawFulfillRandomnessCall__Outputs(this);
  }
}

export class RawFulfillRandomnessCall__Inputs {
  _call: RawFulfillRandomnessCall;

  constructor(call: RawFulfillRandomnessCall) {
    this._call = call;
  }

  get requestId(): Bytes {
    return this._call.inputValues[0].value.toBytes();
  }

  get randomness(): BigInt {
    return this._call.inputValues[1].value.toBigInt();
  }
}

export class RawFulfillRandomnessCall__Outputs {
  _call: RawFulfillRandomnessCall;

  constructor(call: RawFulfillRandomnessCall) {
    this._call = call;
  }
}

export class RefundNFTsCall extends ethereum.Call {
  get inputs(): RefundNFTsCall__Inputs {
    return new RefundNFTsCall__Inputs(this);
  }

  get outputs(): RefundNFTsCall__Outputs {
    return new RefundNFTsCall__Outputs(this);
  }
}

export class RefundNFTsCall__Inputs {
  _call: RefundNFTsCall;

  constructor(call: RefundNFTsCall) {
    this._call = call;
  }

  get _battleId(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }
}

export class RefundNFTsCall__Outputs {
  _call: RefundNFTsCall;

  constructor(call: RefundNFTsCall) {
    this._call = call;
  }
}

export class RenounceOwnershipCall extends ethereum.Call {
  get inputs(): RenounceOwnershipCall__Inputs {
    return new RenounceOwnershipCall__Inputs(this);
  }

  get outputs(): RenounceOwnershipCall__Outputs {
    return new RenounceOwnershipCall__Outputs(this);
  }
}

export class RenounceOwnershipCall__Inputs {
  _call: RenounceOwnershipCall;

  constructor(call: RenounceOwnershipCall) {
    this._call = call;
  }
}

export class RenounceOwnershipCall__Outputs {
  _call: RenounceOwnershipCall;

  constructor(call: RenounceOwnershipCall) {
    this._call = call;
  }
}

export class RetrieveNFTCall extends ethereum.Call {
  get inputs(): RetrieveNFTCall__Inputs {
    return new RetrieveNFTCall__Inputs(this);
  }

  get outputs(): RetrieveNFTCall__Outputs {
    return new RetrieveNFTCall__Outputs(this);
  }
}

export class RetrieveNFTCall__Inputs {
  _call: RetrieveNFTCall;

  constructor(call: RetrieveNFTCall) {
    this._call = call;
  }

  get _battleId(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }
}

export class RetrieveNFTCall__Outputs {
  _call: RetrieveNFTCall;

  constructor(call: RetrieveNFTCall) {
    this._call = call;
  }
}

export class SetBattleTimeCall extends ethereum.Call {
  get inputs(): SetBattleTimeCall__Inputs {
    return new SetBattleTimeCall__Inputs(this);
  }

  get outputs(): SetBattleTimeCall__Outputs {
    return new SetBattleTimeCall__Outputs(this);
  }
}

export class SetBattleTimeCall__Inputs {
  _call: SetBattleTimeCall;

  constructor(call: SetBattleTimeCall) {
    this._call = call;
  }

  get _time(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }
}

export class SetBattleTimeCall__Outputs {
  _call: SetBattleTimeCall;

  constructor(call: SetBattleTimeCall) {
    this._call = call;
  }
}

export class SetChainlinkFeeCall extends ethereum.Call {
  get inputs(): SetChainlinkFeeCall__Inputs {
    return new SetChainlinkFeeCall__Inputs(this);
  }

  get outputs(): SetChainlinkFeeCall__Outputs {
    return new SetChainlinkFeeCall__Outputs(this);
  }
}

export class SetChainlinkFeeCall__Inputs {
  _call: SetChainlinkFeeCall;

  constructor(call: SetChainlinkFeeCall) {
    this._call = call;
  }

  get _fee(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }

  get _digits(): BigInt {
    return this._call.inputValues[1].value.toBigInt();
  }
}

export class SetChainlinkFeeCall__Outputs {
  _call: SetChainlinkFeeCall;

  constructor(call: SetChainlinkFeeCall) {
    this._call = call;
  }
}

export class SetInsuranceFeeCall extends ethereum.Call {
  get inputs(): SetInsuranceFeeCall__Inputs {
    return new SetInsuranceFeeCall__Inputs(this);
  }

  get outputs(): SetInsuranceFeeCall__Outputs {
    return new SetInsuranceFeeCall__Outputs(this);
  }
}

export class SetInsuranceFeeCall__Inputs {
  _call: SetInsuranceFeeCall;

  constructor(call: SetInsuranceFeeCall) {
    this._call = call;
  }

  get _fee(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }
}

export class SetInsuranceFeeCall__Outputs {
  _call: SetInsuranceFeeCall;

  constructor(call: SetInsuranceFeeCall) {
    this._call = call;
  }
}

export class SetInsurancePeriodCall extends ethereum.Call {
  get inputs(): SetInsurancePeriodCall__Inputs {
    return new SetInsurancePeriodCall__Inputs(this);
  }

  get outputs(): SetInsurancePeriodCall__Outputs {
    return new SetInsurancePeriodCall__Outputs(this);
  }
}

export class SetInsurancePeriodCall__Inputs {
  _call: SetInsurancePeriodCall;

  constructor(call: SetInsurancePeriodCall) {
    this._call = call;
  }

  get _period(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }
}

export class SetInsurancePeriodCall__Outputs {
  _call: SetInsurancePeriodCall;

  constructor(call: SetInsurancePeriodCall) {
    this._call = call;
  }
}

export class TransferOwnershipCall extends ethereum.Call {
  get inputs(): TransferOwnershipCall__Inputs {
    return new TransferOwnershipCall__Inputs(this);
  }

  get outputs(): TransferOwnershipCall__Outputs {
    return new TransferOwnershipCall__Outputs(this);
  }
}

export class TransferOwnershipCall__Inputs {
  _call: TransferOwnershipCall;

  constructor(call: TransferOwnershipCall) {
    this._call = call;
  }

  get newOwner(): Address {
    return this._call.inputValues[0].value.toAddress();
  }
}

export class TransferOwnershipCall__Outputs {
  _call: TransferOwnershipCall;

  constructor(call: TransferOwnershipCall) {
    this._call = call;
  }
}

export class WithdrawBalanceCall extends ethereum.Call {
  get inputs(): WithdrawBalanceCall__Inputs {
    return new WithdrawBalanceCall__Inputs(this);
  }

  get outputs(): WithdrawBalanceCall__Outputs {
    return new WithdrawBalanceCall__Outputs(this);
  }
}

export class WithdrawBalanceCall__Inputs {
  _call: WithdrawBalanceCall;

  constructor(call: WithdrawBalanceCall) {
    this._call = call;
  }

  get _target(): Address {
    return this._call.inputValues[0].value.toAddress();
  }

  get _token(): Address {
    return this._call.inputValues[1].value.toAddress();
  }

  get _amount(): BigInt {
    return this._call.inputValues[2].value.toBigInt();
  }
}

export class WithdrawBalanceCall__Outputs {
  _call: WithdrawBalanceCall;

  constructor(call: WithdrawBalanceCall) {
    this._call = call;
  }
}
