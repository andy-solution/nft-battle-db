// THIS IS AN AUTOGENERATED FILE. DO NOT EDIT THIS FILE DIRECTLY.

import {
  ethereum,
  JSONValue,
  TypedMap,
  Entity,
  Bytes,
  Address,
  BigInt
} from "@graphprotocol/graph-ts";

export class ClaimedGFX extends ethereum.Event {
  get params(): ClaimedGFX__Params {
    return new ClaimedGFX__Params(this);
  }
}

export class ClaimedGFX__Params {
  _event: ClaimedGFX;

  constructor(event: ClaimedGFX) {
    this._event = event;
  }

  get nftOwner(): Address {
    return this._event.parameters[0].value.toAddress();
  }

  get amount(): BigInt {
    return this._event.parameters[1].value.toBigInt();
  }
}

export class ClaimedNFT extends ethereum.Event {
  get params(): ClaimedNFT__Params {
    return new ClaimedNFT__Params(this);
  }
}

export class ClaimedNFT__Params {
  _event: ClaimedNFT;

  constructor(event: ClaimedNFT) {
    this._event = event;
  }

  get nftId(): BigInt {
    return this._event.parameters[0].value.toBigInt();
  }

  get nftOwner(): Address {
    return this._event.parameters[1].value.toAddress();
  }
}

export class NewNFTStaked extends ethereum.Event {
  get params(): NewNFTStaked__Params {
    return new NewNFTStaked__Params(this);
  }
}

export class NewNFTStaked__Params {
  _event: NewNFTStaked;

  constructor(event: NewNFTStaked) {
    this._event = event;
  }

  get nftId(): BigInt {
    return this._event.parameters[0].value.toBigInt();
  }

  get nftOwner(): Address {
    return this._event.parameters[1].value.toAddress();
  }

  get nftImage(): string {
    return this._event.parameters[2].value.toString();
  }

  get nftPower(): BigInt {
    return this._event.parameters[3].value.toBigInt();
  }

  get lastStakedTime(): BigInt {
    return this._event.parameters[4].value.toBigInt();
  }
}

export class OwnershipTransferred extends ethereum.Event {
  get params(): OwnershipTransferred__Params {
    return new OwnershipTransferred__Params(this);
  }
}

export class OwnershipTransferred__Params {
  _event: OwnershipTransferred;

  constructor(event: OwnershipTransferred) {
    this._event = event;
  }

  get previousOwner(): Address {
    return this._event.parameters[0].value.toAddress();
  }

  get newOwner(): Address {
    return this._event.parameters[1].value.toAddress();
  }
}

export class NFTStaking__stakedNftsResult {
  value0: BigInt;
  value1: Address;
  value2: string;
  value3: BigInt;
  value4: BigInt;

  constructor(
    value0: BigInt,
    value1: Address,
    value2: string,
    value3: BigInt,
    value4: BigInt
  ) {
    this.value0 = value0;
    this.value1 = value1;
    this.value2 = value2;
    this.value3 = value3;
    this.value4 = value4;
  }

  toMap(): TypedMap<string, ethereum.Value> {
    let map = new TypedMap<string, ethereum.Value>();
    map.set("value0", ethereum.Value.fromUnsignedBigInt(this.value0));
    map.set("value1", ethereum.Value.fromAddress(this.value1));
    map.set("value2", ethereum.Value.fromString(this.value2));
    map.set("value3", ethereum.Value.fromUnsignedBigInt(this.value3));
    map.set("value4", ethereum.Value.fromUnsignedBigInt(this.value4));
    return map;
  }
}

export class NFTStaking extends ethereum.SmartContract {
  static bind(address: Address): NFTStaking {
    return new NFTStaking("NFTStaking", address);
  }

  getRewardsByNftId(_nftId: BigInt): BigInt {
    let result = super.call(
      "getRewardsByNftId",
      "getRewardsByNftId(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(_nftId)]
    );

    return result[0].toBigInt();
  }

  try_getRewardsByNftId(_nftId: BigInt): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "getRewardsByNftId",
      "getRewardsByNftId(uint256):(uint256)",
      [ethereum.Value.fromUnsignedBigInt(_nftId)]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  getRewardsByOwner(_owner: Address): BigInt {
    let result = super.call(
      "getRewardsByOwner",
      "getRewardsByOwner(address):(uint256)",
      [ethereum.Value.fromAddress(_owner)]
    );

    return result[0].toBigInt();
  }

  try_getRewardsByOwner(_owner: Address): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "getRewardsByOwner",
      "getRewardsByOwner(address):(uint256)",
      [ethereum.Value.fromAddress(_owner)]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }

  nftOwners(param0: BigInt): Address {
    let result = super.call("nftOwners", "nftOwners(uint256):(address)", [
      ethereum.Value.fromUnsignedBigInt(param0)
    ]);

    return result[0].toAddress();
  }

  try_nftOwners(param0: BigInt): ethereum.CallResult<Address> {
    let result = super.tryCall("nftOwners", "nftOwners(uint256):(address)", [
      ethereum.Value.fromUnsignedBigInt(param0)
    ]);
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toAddress());
  }

  owner(): Address {
    let result = super.call("owner", "owner():(address)", []);

    return result[0].toAddress();
  }

  try_owner(): ethereum.CallResult<Address> {
    let result = super.tryCall("owner", "owner():(address)", []);
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toAddress());
  }

  stakedNfts(param0: Address, param1: BigInt): NFTStaking__stakedNftsResult {
    let result = super.call(
      "stakedNfts",
      "stakedNfts(address,uint256):(uint256,address,string,uint256,uint256)",
      [
        ethereum.Value.fromAddress(param0),
        ethereum.Value.fromUnsignedBigInt(param1)
      ]
    );

    return new NFTStaking__stakedNftsResult(
      result[0].toBigInt(),
      result[1].toAddress(),
      result[2].toString(),
      result[3].toBigInt(),
      result[4].toBigInt()
    );
  }

  try_stakedNfts(
    param0: Address,
    param1: BigInt
  ): ethereum.CallResult<NFTStaking__stakedNftsResult> {
    let result = super.tryCall(
      "stakedNfts",
      "stakedNfts(address,uint256):(uint256,address,string,uint256,uint256)",
      [
        ethereum.Value.fromAddress(param0),
        ethereum.Value.fromUnsignedBigInt(param1)
      ]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(
      new NFTStaking__stakedNftsResult(
        value[0].toBigInt(),
        value[1].toAddress(),
        value[2].toString(),
        value[3].toBigInt(),
        value[4].toBigInt()
      )
    );
  }

  stakedNftsCount(param0: Address): BigInt {
    let result = super.call(
      "stakedNftsCount",
      "stakedNftsCount(address):(uint256)",
      [ethereum.Value.fromAddress(param0)]
    );

    return result[0].toBigInt();
  }

  try_stakedNftsCount(param0: Address): ethereum.CallResult<BigInt> {
    let result = super.tryCall(
      "stakedNftsCount",
      "stakedNftsCount(address):(uint256)",
      [ethereum.Value.fromAddress(param0)]
    );
    if (result.reverted) {
      return new ethereum.CallResult();
    }
    let value = result.value;
    return ethereum.CallResult.fromValue(value[0].toBigInt());
  }
}

export class ConstructorCall extends ethereum.Call {
  get inputs(): ConstructorCall__Inputs {
    return new ConstructorCall__Inputs(this);
  }

  get outputs(): ConstructorCall__Outputs {
    return new ConstructorCall__Outputs(this);
  }
}

export class ConstructorCall__Inputs {
  _call: ConstructorCall;

  constructor(call: ConstructorCall) {
    this._call = call;
  }
}

export class ConstructorCall__Outputs {
  _call: ConstructorCall;

  constructor(call: ConstructorCall) {
    this._call = call;
  }
}

export class ClaimAllNftsCall extends ethereum.Call {
  get inputs(): ClaimAllNftsCall__Inputs {
    return new ClaimAllNftsCall__Inputs(this);
  }

  get outputs(): ClaimAllNftsCall__Outputs {
    return new ClaimAllNftsCall__Outputs(this);
  }
}

export class ClaimAllNftsCall__Inputs {
  _call: ClaimAllNftsCall;

  constructor(call: ClaimAllNftsCall) {
    this._call = call;
  }
}

export class ClaimAllNftsCall__Outputs {
  _call: ClaimAllNftsCall;

  constructor(call: ClaimAllNftsCall) {
    this._call = call;
  }
}

export class ClaimGFXRewardsCall extends ethereum.Call {
  get inputs(): ClaimGFXRewardsCall__Inputs {
    return new ClaimGFXRewardsCall__Inputs(this);
  }

  get outputs(): ClaimGFXRewardsCall__Outputs {
    return new ClaimGFXRewardsCall__Outputs(this);
  }
}

export class ClaimGFXRewardsCall__Inputs {
  _call: ClaimGFXRewardsCall;

  constructor(call: ClaimGFXRewardsCall) {
    this._call = call;
  }
}

export class ClaimGFXRewardsCall__Outputs {
  _call: ClaimGFXRewardsCall;

  constructor(call: ClaimGFXRewardsCall) {
    this._call = call;
  }
}

export class ClaimNFTByIdCall extends ethereum.Call {
  get inputs(): ClaimNFTByIdCall__Inputs {
    return new ClaimNFTByIdCall__Inputs(this);
  }

  get outputs(): ClaimNFTByIdCall__Outputs {
    return new ClaimNFTByIdCall__Outputs(this);
  }
}

export class ClaimNFTByIdCall__Inputs {
  _call: ClaimNFTByIdCall;

  constructor(call: ClaimNFTByIdCall) {
    this._call = call;
  }

  get _nftId(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }
}

export class ClaimNFTByIdCall__Outputs {
  _call: ClaimNFTByIdCall;

  constructor(call: ClaimNFTByIdCall) {
    this._call = call;
  }
}

export class InitializeCall extends ethereum.Call {
  get inputs(): InitializeCall__Inputs {
    return new InitializeCall__Inputs(this);
  }

  get outputs(): InitializeCall__Outputs {
    return new InitializeCall__Outputs(this);
  }
}

export class InitializeCall__Inputs {
  _call: InitializeCall;

  constructor(call: InitializeCall) {
    this._call = call;
  }

  get _nftContractAddress(): Address {
    return this._call.inputValues[0].value.toAddress();
  }

  get _gfxContractAddress(): Address {
    return this._call.inputValues[1].value.toAddress();
  }
}

export class InitializeCall__Outputs {
  _call: InitializeCall;

  constructor(call: InitializeCall) {
    this._call = call;
  }
}

export class RenounceOwnershipCall extends ethereum.Call {
  get inputs(): RenounceOwnershipCall__Inputs {
    return new RenounceOwnershipCall__Inputs(this);
  }

  get outputs(): RenounceOwnershipCall__Outputs {
    return new RenounceOwnershipCall__Outputs(this);
  }
}

export class RenounceOwnershipCall__Inputs {
  _call: RenounceOwnershipCall;

  constructor(call: RenounceOwnershipCall) {
    this._call = call;
  }
}

export class RenounceOwnershipCall__Outputs {
  _call: RenounceOwnershipCall;

  constructor(call: RenounceOwnershipCall) {
    this._call = call;
  }
}

export class SetClaimRateCall extends ethereum.Call {
  get inputs(): SetClaimRateCall__Inputs {
    return new SetClaimRateCall__Inputs(this);
  }

  get outputs(): SetClaimRateCall__Outputs {
    return new SetClaimRateCall__Outputs(this);
  }
}

export class SetClaimRateCall__Inputs {
  _call: SetClaimRateCall;

  constructor(call: SetClaimRateCall) {
    this._call = call;
  }

  get _rate(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }

  get _digits(): BigInt {
    return this._call.inputValues[1].value.toBigInt();
  }
}

export class SetClaimRateCall__Outputs {
  _call: SetClaimRateCall;

  constructor(call: SetClaimRateCall) {
    this._call = call;
  }
}

export class StakeNFTCall extends ethereum.Call {
  get inputs(): StakeNFTCall__Inputs {
    return new StakeNFTCall__Inputs(this);
  }

  get outputs(): StakeNFTCall__Outputs {
    return new StakeNFTCall__Outputs(this);
  }
}

export class StakeNFTCall__Inputs {
  _call: StakeNFTCall;

  constructor(call: StakeNFTCall) {
    this._call = call;
  }

  get _nftId(): BigInt {
    return this._call.inputValues[0].value.toBigInt();
  }
}

export class StakeNFTCall__Outputs {
  _call: StakeNFTCall;

  constructor(call: StakeNFTCall) {
    this._call = call;
  }
}

export class TransferOwnershipCall extends ethereum.Call {
  get inputs(): TransferOwnershipCall__Inputs {
    return new TransferOwnershipCall__Inputs(this);
  }

  get outputs(): TransferOwnershipCall__Outputs {
    return new TransferOwnershipCall__Outputs(this);
  }
}

export class TransferOwnershipCall__Inputs {
  _call: TransferOwnershipCall;

  constructor(call: TransferOwnershipCall) {
    this._call = call;
  }

  get newOwner(): Address {
    return this._call.inputValues[0].value.toAddress();
  }
}

export class TransferOwnershipCall__Outputs {
  _call: TransferOwnershipCall;

  constructor(call: TransferOwnershipCall) {
    this._call = call;
  }
}
