import { BigInt, ByteArray, Bytes } from "@graphprotocol/graph-ts"
import {
  GetWeapon,
  NewNFTMinted,
  Transfer,
} from "../generated/NFTV2Contract/NFTV2Contract"
import { Nft } from "../generated/schema"

export function handleNewNFTMinted(event: NewNFTMinted): void {
  let nftId = event.params.id.toHexString();
  let owner = event.params.owner.toHexString();

  let nft = new Nft(nftId)
  nft.nftId = event.params.id;
  nft.uri = event.params.uri;
  nft.owner = ByteArray.fromHexString(owner) as Bytes;
  nft.level = BigInt.fromI32(1);
  nft.power = BigInt.fromI32(0);
  nft.name = event.params.name;
  nft.save();
}

export function handleGetWeapon(event: GetWeapon): void {
  let nftId = event.params.nftId.toHexString();
  let nft = Nft.load(nftId);

  if (nft == null) return;

  nft.power = event.params.power;
  nft.save();
}

export function handleTransfer(event: Transfer): void {
  let nftId = event.params.tokenId.toHexString();
  let nft = Nft.load(nftId);

  if (nft == null) return;

  let owner = event.params.to.toHexString();
  nft.owner = ByteArray.fromHexString(owner) as Bytes;
  nft.save();
}
