import { BigInt, ByteArray, Bytes } from "@graphprotocol/graph-ts";
import {
  ClaimedAward,
  NewParticipantJoined,
} from "../generated/PrivateBattle/PrivateBattle";
import { Nft } from "../generated/schema";

export function handleClaimedAward(event: ClaimedAward): void {
  let nftId = event.params.nftId.toHexString();
  let nft = Nft.load(nftId);
  if (nft == null) return;

  nft.level = nft.level.plus(BigInt.fromI32(1));
  nft.save();
}

export function handleNewParticipantJoined(event: NewParticipantJoined): void {
  let nftId = event.params.nftId.toHexString();
  let nft = Nft.load(nftId);
  if (nft == null) return;

  let owner = event.address.toHexString();
  nft.owner = ByteArray.fromHexString(owner) as Bytes;
  nft.save();
}
