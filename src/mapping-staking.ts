import { ByteArray, Bytes, store } from "@graphprotocol/graph-ts";

import {
  NewNFTStaked,
  ClaimedNFT,
  ClaimedGFX,
} from "../generated/NFTStaking/NFTStaking";
import { StakedNft } from "../generated/schema";

export function handleNewNFTStaked(event: NewNFTStaked): void {
  let nftId = event.params.nftId.toHexString();
  let nftOwner = event.params.nftOwner.toHexString();

  let stakedNftId = nftId + "-" + nftOwner;
  let nft = new StakedNft(stakedNftId);
  nft.nftId = event.params.nftId;
  nft.owner = ByteArray.fromHexString(nftOwner) as Bytes;
  nft.image = event.params.nftImage;
  nft.power = event.params.nftPower;
  nft.lastStakedTime = event.params.lastStakedTime;
  nft.save();
}

export function handleClaimedNFT(event: ClaimedNFT): void {
  let nftId = event.params.nftId.toHexString();
  let nftOwner = event.params.nftOwner.toHexString();

  let stakedNftId = nftId + "-" + nftOwner;
  store.remove("StakedNft", stakedNftId);
}
