import { BigInt, ByteArray, Bytes } from "@graphprotocol/graph-ts"
import {
  ClaimedNFT,
  ClaimedPresent,
  CompleteBattle,
  NewBattleCreated,
  NewParticipantJoined,
  NewWinner,
} from "../generated/BattleV2Contract/BattleV2Contract"
import { Battle, Participant, Winner, Nft } from "../generated/schema"

export function handleClaimedNFT(event: ClaimedNFT): void {
  let nftId = event.params.nftId.toHexString();
  let nftOwner = event.params.nftOwner.toHexString();
  let participantId = nftId + '-' + nftOwner;
  let participant = Participant.load(participantId);
  if (participant == null) return;

  participant.isClamed = true;
  participant.save();
  
  let nft = Nft.load(nftId)
  if (nft == null) return;

  nft.owner = ByteArray.fromHexString(nftOwner) as Bytes;
  nft.save();
}

export function handleClaimedPresent(event: ClaimedPresent): void {
  let nftId = event.params.nftId.toHexString();
  let nftOwner = event.params.nftOwner.toHexString();
  let winnerId = nftId + '-' + nftOwner;
  let winner = Winner.load(winnerId);
  if (winner == null) return;

  winner.isClamed = true;
  winner.save();

  let nft = Nft.load(nftId)
  if (nft == null) return;

  nft.owner = ByteArray.fromHexString(nftOwner) as Bytes;
  nft.save();
}

export function handleCompleteBattle(event: CompleteBattle): void {
  let battleId = event.params.battleId.toHexString();
  let battle = Battle.load(battleId);
  if (battle == null) return;

  battle.status = "END";
  battle.save();
}

export function handleNewBattleCreated(event: NewBattleCreated): void {
  let battleId = event.params.battleId.toHexString();
  let battle = new Battle(battleId);
  battle.battleId = event.params.battleId;
  battle.startTime = event.params.startTime;
  battle.endTime = event.params.endTime;
  battle.level = event.params.level;
  battle.award = event.params.award;
  battle.uri = event.params.uri;
  battle.status = "READY";
  battle.total = BigInt.fromI32(0);
  battle.save();
}

export function handleNewParticipantJoined(event: NewParticipantJoined): void {
  let battleId = event.params.battleId.toHexString();
  let battle = Battle.load(battleId);
  if (battle == null) return;

  battle.total = event.params.total;
  battle.save();

  let nftId = event.params.nftId.toHexString();

  let nftOwner = event.transaction.from.toHexString();
  let participantId = nftId + '-' + nftOwner;
  let participant = new Participant(participantId);
  participant.nftId = event.params.nftId;
  participant.nftOwner = ByteArray.fromHexString(nftOwner) as Bytes;
  participant.isClamed = false;
  participant.battle = battle.id;
  participant.save();

  let nft = Nft.load(nftId);
  if (nft == null) return;

  let owner = event.address.toHexString();
  nft.owner = ByteArray.fromHexString(owner) as Bytes;
  nft.save();
}

export function handleNewWinner(event: NewWinner): void {
  let battleId = event.params.battleId.toHexString();
  let battle = Battle.load(battleId);
  if (battle == null) return;

  let nftId = event.params.nftId.toHexString();

  let nftOwner = event.params.nftOwner.toHexString();
  let winnerId = nftId + '-' + nftOwner;
  let winner = new Winner(winnerId);
  winner.battle = battle.id;
  winner.nftId = event.params.nftId;
  winner.nftOwner = ByteArray.fromHexString(nftOwner) as Bytes;
  winner.isClamed = false;
  winner.save();

  let nft = Nft.load(nftId);
  if (nft == null) return;

  nft.uri = battle.uri;
  nft.level = nft.level.plus(BigInt.fromI32(1));
  nft.save();
}
